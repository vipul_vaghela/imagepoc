var SelectedImage;
var canvas, canvasCombine, ctxCombine;
var left, right, center;
var left_blank, right_blank, center_blank;

function loadAllCanvas() {
    canvasCombine = document.getElementById("canvasAll");
    ctxCombine = canvasCombine.getContext("2d");

    canvas = new fabric.Canvas('imageCanvas', {
        backgroundColor: 'rgb(240,240,240)'
    });
//            canvas.setBackgroundImage('https://duw6wk4ou1qt.cloudfront.net/files/v469/generator/40/11oz_template.png', canvas.renderAll.bind(canvas));
    canvas.setBackgroundImage('template.png', canvas.renderAll.bind(canvas));


    var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
    canvas.on({
        'object:moving': function (e) {
            // console.log("object moved...");
        },
        'object:modified': function (e) {
            // console.log(e);
            //console.log("object modified...");
        },
        'object:rotating': function (e) {
//            console.log("object rotating...");
//            console.log(e.target);
        }
    });
    imageLoader.addEventListener("mouseover", function (event) {
//        console.log("called");
    });
    var imageSaver = document.getElementById('imageSaver');
    imageSaver.addEventListener('click', saveImage, false);

    var removeImage1 = document.getElementById('imageRemove1');
    removeImage1.addEventListener('click', removeImage, false);

    canvas.backgroundImage = 0;

    var img = canvas.toDataURL({
        format: 'jpeg',
        quality: 0.8
    });

    var myImg = document.getElementById('myImg');
    //  myImg.src = img;

    canvas1(img, true);
    canvas2(img, true);
    canvas3(img, true);
    setTimeout(function () {
        // canvas.setBackgroundImage('template.png', canvas.renderAll.bind(canvas));
        console.log("call finished...");
        //combineAll(img);
    }, 5000);
}

function combineAll(img) {

    if (ctxCombine) {
        ctxCombine.clearRect(0, 0, canvasCombine.width, canvasCombine.height);
    }
//First canvas data
    var img1 = loadImage(left, main);
//Second canvas data
    var img2 = loadImage(center, main);

    var img3 = loadImage(right, main);

    var imagesLoaded = 0;

    function main() {
        imagesLoaded += 1;
        var iw = img1.width;
        var ih = img1.height;
        if (imagesLoaded == 3) {
            // composite now
            var diff = resemble(left).compareTo(left_blank).ignoreColors().onComplete(function (data) {
                console.log(data);
                if (data.misMatchPercentage !== "0.00") {
                    ctxCombine.drawImage(img1, 0, 0, iw, ih);
                }
                /*
                 {
                 misMatchPercentage : 100, // %
                 isSameDimensions: true, // or false
                 dimensionDifference: { width: 0, height: -1 }, // defined if dimensions are not the same
                 getImageDataUrl: function(){}
                 }
                 */
            });
            var diff = resemble(center).compareTo(center_blank).ignoreColors().onComplete(function (data) {
                console.log(data);
                if (data.misMatchPercentage !== "0.00") {
                    ctxCombine.drawImage(img2, iw, 0, iw, ih);
                }
                /*
                 {
                 misMatchPercentage : 100, // %
                 isSameDimensions: true, // or false
                 dimensionDifference: { width: 0, height: -1 }, // defined if dimensions are not the same
                 getImageDataUrl: function(){}
                 }
                 */
            });
            var diff = resemble(right).compareTo(right_blank).ignoreColors().onComplete(function (data) {
                console.log(data);
                if (data.misMatchPercentage !== "0.00") {
                    ctxCombine.drawImage(img3, iw + iw, 0, img3.width, ih);
                }
                /*
                 {
                 misMatchPercentage : 100, // %
                 isSameDimensions: true, // or false
                 dimensionDifference: { width: 0, height: -1 }, // defined if dimensions are not the same
                 getImageDataUrl: function(){}
                 }
                 */
            });

        }

    }

    function loadImage(src, onload) {
//        console.log('loadImage', src);
        var img = new Image();

        img.onload = onload;
        img.src = src;

        return img;
    }
}
function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var img = new Image();
        img.onload = function () {
            var imgInstance = new fabric.Image(img, {
                scaleX: 0.2,
                scaleY: 0.2
            })
            canvas.add(imgInstance);
            canvas.setActiveObject(imgInstance);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    SelectedImage = reader;
}

function removeImage(e) {

    var object = canvas.getActiveObject();
    if (!object) {
        alert('Please select the element to remove');
        return '';
    }
    canvas.remove(object);
}

function saveImage(e) {

    canvas.backgroundImage = 0;

    var img = canvas.toDataURL({
        format: 'jpeg',
        quality: 0.8
    });

    var myImg = document.getElementById('myImg');
    //  myImg.src = img;
    left = "";
    right = "";
    center = "";
    console.log(ctxCombine);
    if (ctxCombine) {
        ctxCombine.clearRect(0, 0, canvasCombine.width, canvasCombine.height);
    }
    canvas1(img);
    canvas2(img);
    canvas3(img);
    setTimeout(function () {
        canvas.setBackgroundImage('template.png', canvas.renderAll.bind(canvas));
        combineAll(img);
        console.log("combine finished...");
    }, 5000);
    //}, 10);
}

function canvas1(img, isBlank) {
    var canvas = document.getElementById("canvas1");
    var ctx = canvas.getContext("2d");

    var productImg = new Image();

    productImg.onload = function () {
        var iw = productImg.width;
        var ih = productImg.height;

        canvas.width = iw;
        canvas.height = ih;

        ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                0, 0, iw, ih);
        loadUpperIMage(img).then(function(data){
            console.log(data);
        })

    };

    productImg.src = "left.png";//http://res.cloudinary.com/pussyhunter/image/upload/c_scale,f_auto,h_350/left_handle_cup_i7ztfs.jpg"


    function loadUpperIMage(img1) {
        var img = new Image();
        img.src = img1;//"r.jpeg";//SelectedImage.result;// "2.png";//"http://res.cloudinary.com/pussyhunter/image/upload/v1488184107/500_F_97150423_M13q2FeAUZxxIx6CaPixHupprmyiVVli_skh6fe.jpg"
        //    img.src = "http://res.cloudinary.com/pussyhunter/image/upload/v1488184107/500_F_97150423_M13q2FeAUZxxIx6CaPixHupprmyiVVli_skh6fe.jpg"
        img.onload = function () {
            var iw = img.width;
            var ih = img.height;
            var xOffset = 102, //left padding
                    yOffset = 110; //top padding

            var a = 75.0; //image width
            var b = 10; //round ness

            var scaleFactor = iw / (4 * a);

            // draw vertical slices
            var counter = 0;
            ctx.globalAlpha = 0.9;
            for (var X = 0; X < iw; X += 1) {
                var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                ctx.globalAlpha = 0.9;
                //void ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
                ctx.drawImage(img, X * scaleFactor, 0, iw / 9, ih, X + xOffset, y + yOffset, 1, 174);
                ctx.globalCompositeOperation = "multiply";

                if (isBlank == true) {
                    left_blank = canvas.toDataURL({
                        format: 'jpeg',
                        quality: 0.8
                    });
                } else {
                    left = canvas.toDataURL({
                        format: 'jpeg',
                        quality: 0.8
                    });
                }
            }
           return new Promise(
                    function (resolve, reject) {
                         resolve("finished");
                    });

        };
    }

}
;
function canvas2(img1, isBlank) {

    var canvas = document.getElementById("canvas2");
    var ctx = canvas.getContext("2d");

    var productImg = new Image();
    productImg.onload = function () {
        var iw = productImg.width;
        var ih = productImg.height;

        canvas.width = iw;
        canvas.height = ih;

        ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                0, 0, iw, ih);
        loadUpperIMage(img1)
    };


    productImg.src = "center.jpg";//"http://res.cloudinary.com/pussyhunter/image/upload/h_350/canter_handle_cup_xyxhdd.jpg"

    function loadUpperIMage(img1) {
        var img = new Image();

        img.src = img1;//"argusoft.png";//"http://res.cloudinary.com/pussyhunter/image/upload/v1488184107/500_F_97150423_M13q2FeAUZxxIx6CaPixHupprmyiVVli_skh6fe.jpg"
        img.onload = function () {

            var iw = img.width;
            var ih = img.height;

            // alert(iw)

            var xOffset = 101, //left padding
                    yOffset = 110; //top padding

            var a = 75.0; //image width
            var b = 10; //round ness

            var scaleFactor = iw / (4 * a);

            // draw vertical slices
            for (var X = 0; X < iw; X += 1) {
                var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                ctx.globalAlpha = 0.9;
                ctx.drawImage(img, X * scaleFactor, 0, iw / 3, ih, X + xOffset, y + yOffset, 1, 174);
                ctx.globalCompositeOperation = "multiply";

                if (isBlank == true) {
                    center_blank = canvas.toDataURL({
                        format: 'jpeg',
                        quality: 0.8
                    });
                } else {
                    center = canvas.toDataURL({
                        format: 'jpeg',
                        quality: 0.8
                    });
                }

            }

        };
    }

}
;

function canvas3(img1, isBlank) {

    var canvas = document.getElementById("canvas3");
    var ctx = canvas.getContext("2d");

    var productImg = new Image();
    productImg.onload = function () {
        var iw = productImg.width;
        var ih = productImg.height;

        canvas.width = iw;
        canvas.height = ih;

        ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                0, 0, iw, ih);
        loadUpperIMage(img1)
    };

    productImg.src = "right.jpg";//"http://res.cloudinary.com/pussyhunter/image/upload/h_350/right_handle_cup_dsdhr7.jpg"


    function loadUpperIMage(img1) {
        var img = new Image();

        img.src = img1;//"argusoft.png";//"http://res.cloudinary.com/pussyhunter/image/upload/v1488184107/500_F_97150423_M13q2FeAUZxxIx6CaPixHupprmyiVVli_skh6fe.jpg"

        img.onload = function () {

            var iw = img.width;
            var ih = img.height;

            //alert(iw)

            var xOffset = 102, //left padding
                    yOffset = 110; //top padding

            var a = 75.0; //image width
            var b = 10; //round ness

            var scaleFactor = iw / (3 * a);

            // draw vertical slices
            for (var X = 0; X < iw; X += 1) {
                var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                ctx.globalAlpha = 0.9;
                ctx.drawImage(img, X * scaleFactor, 0, iw / 1.5, ih, X + xOffset, y + yOffset, 1, 174);
                ctx.globalCompositeOperation = "multiply";

                if (isBlank == true) {
                    right_blank = canvas.toDataURL({
                        format: 'jpeg',
                        quality: 0.8
                    });
                } else {
                    right = canvas.toDataURL({
                        format: 'jpeg',
                        quality: 0.8
                    });
                }
            }


        };
    }

}
;