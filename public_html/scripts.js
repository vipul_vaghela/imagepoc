var SelectedImage;
var canvas, canvasCombine, ctxCombine;
var left, right, center;


function loadAllCanvas() {
    canvasCombine = document.getElementById("canvasAll");
    ctxCombine = canvasCombine.getContext("2d");

    canvas = new fabric.Canvas('imageCanvas', {
        backgroundColor: 'rgb(240,240,240)'
    });
//            canvas.setBackgroundImage('https://duw6wk4ou1qt.cloudfront.net/files/v469/generator/40/11oz_template.png', canvas.renderAll.bind(canvas));
    canvas.setBackgroundImage('template.png', canvas.renderAll.bind(canvas));


    var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
    canvas.on({
        'object:moving': function (e) {
            // console.log("object moved...");
        },
        'object:modified': function (e) {
            // console.log(e);
            //console.log("object modified...");
        },
        'object:rotating': function (e) {
//            console.log("object rotating...");
//            console.log(e.target);
        }
    });
    imageLoader.addEventListener("mouseover", function (event) {
//        console.log("called");
    });
    var imageSaver = document.getElementById('imageSaver');
    imageSaver.addEventListener('click', saveImage, false);

    var removeImage1 = document.getElementById('imageRemove1');
    removeImage1.addEventListener('click', removeImage, false);

    var downImage = document.getElementById('downloadimage');
    downImage.addEventListener('click', downloadImage, false);


}

function combineAll(img) {

    if (ctxCombine) {
        ctxCombine.clearRect(0, 0, canvasCombine.width, canvasCombine.height);
    }
//First canvas data
    var img1 = loadImage(left, main);
//Second canvas data
    var img2 = loadImage(center, main);

    var img3 = loadImage(right, main);

    var imagesLoaded = 0;

    function main() {
        imagesLoaded += 1;
        var iw = img1.width;
        var ih = img1.height;
        var firstImageFound = false;
        var secondImageFound = false;
        var firstImageFound = false;
        if (imagesLoaded == 3) {
           
            var diff = resemble(left).compareTo("left.png").ignoreColors().onComplete(function (data) {
                console.log(data);
                var misMatchPer = parseFloat(data.misMatchPercentage);
                if (misMatchPer > 0.04) {
                    ctxCombine.drawImage(img1, 0, 0, iw, ih);
                    firstImageFound = true;
                }

            });
            var diff = resemble(center).compareTo("center.jpg").ignoreColors().onComplete(function (data) {

                var misMatchPer = parseFloat(data.misMatchPercentage);
                if (misMatchPer > 0.04) {
                    if (firstImageFound == true) {
                        ctxCombine.drawImage(img2, iw, 0, iw, ih);
                    } else {
                        ctxCombine.drawImage(img2, 0, 0, iw, ih);
                    }
                    secondImageFound = true;
                }
                
            });
            var diff = resemble(right).compareTo("right.jpg").ignoreColors().onComplete(function (data) {

                var misMatchPer = parseFloat(data.misMatchPercentage);
                if (misMatchPer > 0.04) {
                    if (firstImageFound == true && secondImageFound == true) {
                        ctxCombine.drawImage(img3, iw + iw, 0, img3.width, ih);
                    } else if (firstImageFound == false && secondImageFound == false) {
                        ctxCombine.drawImage(img3, 0, 0, iw, ih);
                    } else {
                        ctxCombine.drawImage(img3, iw, 0, iw, ih);
                    }
                }
               
            });

        }

    }

    function loadImage(src, onload) {
        var img = new Image();

        img.onload = onload;
        img.src = src;

        return img;
    }
}
function downloadImage() {
    var link = document.createElement('a');
    link.href = canvasCombine.toDataURL({
        format: 'png',
        quality: 0.8
    });
    link.download = 'finalImage.png';
    document.body.appendChild(link);
    link.click();
}
function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var img = new Image();
        img.onload = function () {
            var imgInstance = new fabric.Image(img, {
                scaleX: 0.2,
                scaleY: 0.2
            })
            canvas.add(imgInstance);
            canvas.setActiveObject(imgInstance);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    SelectedImage = reader;
}

function removeImage(e) {

    var object = canvas.getActiveObject();
    if (!object) {
        alert('Please select the element to remove');
        return '';
    }
    canvas.remove(object);
}
function white2transparent(img){
            var c = document.createElement('canvas');

            var w = img.width, h = img.height;

            c.width = w;
            c.height = h;

            var ctx = c.getContext('2d');
            alert(1);
            ctx.width = w;
            ctx.height = h;
            ctx.drawImage(img, 0, 0, w, h);
            var imageData = ctx.getImageData(0, 0, w, h);
            var pixel = imageData.data;

            var r = 0, g = 1, b = 2, a = 3;
            for (var p = 0; p < pixel.length; p += 4)
            {
                if (
                        pixel[p + r] == 255 &&
                        pixel[p + g] == 255 &&
                        pixel[p + b] == 255) // if white then change alpha to 0
                {
                    pixel[p + a] = 0;
                }
            }

            ctx.putImageData(imageData, 0, 0);
             var myImg = document.getElementById('myImg1');
//    var myImg1 = document.getElementById('myImg1');
   //console.log(myImg1);
//      myImg.src = c.toDataURL('image/png');
            return c.toDataURL('image/png');
        }
function saveImage(e) {

    canvas.backgroundImage = 0;

    var img = canvas.toDataURL({
        format: 'jpeg',
        quality: 0.8
    });
//    console.log(1);
//console.log(img);
//console.log(2);
    var myImg = document.getElementById('myImg');
//    var myImg1 = document.getElementById('myImg1');
   //console.log(myImg1);
//      myImg.src = img;
    left = "";
    right = "";
    center = "";
    if (ctxCombine) {
        ctxCombine.clearRect(0, 0, canvasCombine.width, canvasCombine.height);
    }
    
    canvas1(img);
    canvas2(img);
    canvas3(img);
    setTimeout(function () {
        canvas.setBackgroundImage('template.png', canvas.renderAll.bind(canvas));
        combineAll(img);
         myImg.src = white2transparent(myImg);
    }, 600);
    //}, 10);
}

function canvas1(img, isBlank) {
    var canvas = document.createElement("canvas");//document.getElementById("canvas1");
    var ctx = canvas.getContext("2d");

    var productImg = new Image();

    productImg.onload = function () {
        var iw = productImg.width;
        var ih = productImg.height;

        canvas.width = iw;
        canvas.height = ih;

        ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                0, 0, iw, ih);
        loadUpperIMage(img);


    };

    productImg.src = "left.png";


    function loadUpperIMage(img1) {
        var img = new Image();
        img.src = img1;
        img.onload = function () {

            var iw = img.width;
            var ih = img.height;
            var xOffset = 102, //left padding
                    yOffset = 110; //top padding

            var a = 75.0; //image width
            var b = 10; //round ness

            var scaleFactor = iw / (4 * a);
            // draw vertical slices
            var counter = 0;
          //  ctx.globalAlpha = 0.9;
            for (var X = 0; X < iw; X += 1) {
                var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                ctx.globalAlpha = 0.9;
                //void ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
                ctx.drawImage(img, X * scaleFactor, 0, iw / 9, ih, X + xOffset, y + yOffset, 1, 174);
                ctx.globalCompositeOperation = "multiply";

            }

            left = canvas.toDataURL({
                format: 'jpeg',
                quality: 0.8
            });

        };
//        return new Promise(
//                function (resolve, reject) {
//                    resolve("finished");
//                });
    }

}
;
function canvas2(img1, isBlank) {

    var canvas = document.createElement("canvas");// document.getElementById("canvas2");
    var ctx = canvas.getContext("2d");

    var productImg = new Image();
    productImg.onload = function () {
        var iw = productImg.width;
        var ih = productImg.height;

        canvas.width = iw;
        canvas.height = ih;

        ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                0, 0, iw, ih);
        loadUpperIMage(img1)
    };


    productImg.src = "center.jpg";

    function loadUpperIMage(img1) {
        var img = new Image();

        img.src = img1;
        img.onload = function () {

            var iw = img.width;
            var ih = img.height;

            // alert(iw)

            var xOffset = 101, //left padding
                    yOffset = 110; //top padding

            var a = 75.0; //image width
            var b = 10; //round ness

            var scaleFactor = iw / (4 * a);

            // draw vertical slices
            for (var X = 0; X < iw; X += 1) {
                var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                ctx.globalAlpha = 0.9;
                ctx.drawImage(img, X * scaleFactor, 0, iw / 3, ih, X + xOffset, y + yOffset, 1, 174);
                ctx.globalCompositeOperation = "multiply";



            }

            center = canvas.toDataURL({
                format: 'jpeg',
                quality: 0.8
            });


        };
    }

}
;

function canvas3(img1, isBlank) {

    var canvas = document.createElement("canvas");// document.getElementById("canvas3");
    var ctx = canvas.getContext("2d");

    var productImg = new Image();
    productImg.onload = function () {
        var iw = productImg.width;
        var ih = productImg.height;

        canvas.width = iw;
        canvas.height = ih;

        ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                0, 0, iw, ih);
        loadUpperIMage(img1)
    };

    productImg.src = "right.jpg";


    function loadUpperIMage(img1) {
        var img = new Image();

        img.src = img1;

        img.onload = function () {

            var iw = img.width;
            var ih = img.height;

            //alert(iw)

            var xOffset = 102, //left padding
                    yOffset = 110; //top padding

            var a = 75.0; //image width
            var b = 10; //round ness

            var scaleFactor = iw / (3 * a);

            // draw vertical slices
            for (var X = 0; X < iw; X += 1) {
                var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                ctx.globalAlpha = 0.9;
                ctx.drawImage(img, X * scaleFactor, 0, iw / 1.5, ih, X + xOffset, y + yOffset, 1, 174);
                ctx.globalCompositeOperation = "multiply";


            }
            right = canvas.toDataURL({
                format: 'jpeg',
                quality: 0.8
            });


        };
    }

}
;