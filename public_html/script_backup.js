var SelectedImage;
var canvas;
var left, right, center;

function loadAllCanvas() {
    canvas = new fabric.Canvas('imageCanvas', {
        backgroundColor: 'rgb(240,240,240)'
    });
//            canvas.setBackgroundImage('https://duw6wk4ou1qt.cloudfront.net/files/v469/generator/40/11oz_template.png', canvas.renderAll.bind(canvas));
    canvas.setBackgroundImage('template.png', canvas.renderAll.bind(canvas));


    var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
    canvas.on({
        'object:moving': function (e) {
            // console.log("object moved...");
        },
        'object:modified': function (e) {
            // console.log(e);
            //console.log("object modified...");
        },
        'object:rotating': function (e) {
//            console.log("object rotating...");
//            console.log(e.target);
        }
    });
    imageLoader.addEventListener("mouseover", function (event) {
//        console.log("called");
    });
    var imageSaver = document.getElementById('imageSaver');
    imageSaver.addEventListener('click', saveImage, false);

    var removeImage1 = document.getElementById('imageRemove1');
    removeImage1.addEventListener('click', removeImage, false);
}

function combineAll(img) {
    var canvas = document.getElementById("canvasAll");
    var ctx = canvas.getContext("2d");
//First canvas data
    var img1 = loadImage(left, main);
//Second canvas data
    var img2 = loadImage(center, main);

    var img3 = loadImage(right, main);

    var imagesLoaded = 0;

    function main() {
        imagesLoaded += 1;
        var iw = img1.width;
        var ih = img1.height;
        if (imagesLoaded == 2) {
            // composite now
            ctx.drawImage(img1, 0, 0, iw, ih);

            // ctx.globalAlpha = 0.5;
            ctx.drawImage(img2, iw, 0, iw, ih);

            ctx.drawImage(img3, iw + iw, 0, img3.width, ih);
        }
        var diff = resemble(center).compareTo("blank_center.png").ignoreColors().onComplete(function (data) {
            console.log(data);
            /*
             {
             misMatchPercentage : 100, // %
             isSameDimensions: true, // or false
             dimensionDifference: { width: 0, height: -1 }, // defined if dimensions are not the same
             getImageDataUrl: function(){}
             }
             */
        });
    }

    function loadImage(src, onload) {
//        console.log('loadImage', src);
        var img = new Image();

        img.onload = onload;
        img.src = src;

        return img;
    }
}
function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var img = new Image();
        img.onload = function () {
            var imgInstance = new fabric.Image(img, {
                scaleX: 0.2,
                scaleY: 0.2
            })
            canvas.add(imgInstance);
            canvas.setActiveObject(imgInstance);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    SelectedImage = reader;
}

function removeImage(e) {

    var object = canvas.getActiveObject();
    if (!object) {
        alert('Please select the element to remove');
        return '';
    }
    canvas.remove(object);
}

function saveImage(e) {
//                 this.href = canvas.toDataURL({
//                 format: 'jpeg',
//                 quality: 0.8
//                 });
    //this.download = 'test.jpeg'

    canvas.backgroundImage = 0;
    //  canvas.setBackgroundColor('rgb(254,254,254)', canvas.renderAll.bind(canvas));
    //canvas.setBackgroundColor(null, canvas.renderAll.bind(canvas));
    var img = canvas.toDataURL({
        format: 'jpeg',
        quality: 0.8
    });
    //console.log(SelectedImage);
    //  canvas.setOverlayImage('', canvas.renderAll.bind(canvas));
    //demo();
    // setTimeout(function () {
    var myImg = document.getElementById('myImg');
    //  myImg.src = img;

    canvas1(img);
    canvas2(img);
    canvas3(img);
    setTimeout(function () {
        canvas.setBackgroundImage('template.png', canvas.renderAll.bind(canvas));
        combineAll(img);
    }, 100);
    //}, 10);
}

function canvas1(img) {
    var canvas = document.getElementById("canvas1");
    var ctx = canvas.getContext("2d");

    var productImg = new Image();

    productImg.onload = function () {
        var iw = productImg.width;
        var ih = productImg.height;

        canvas.width = iw;
        canvas.height = ih;

        ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                0, 0, iw, ih);
        loadUpperIMage(img)

    };

    productImg.src = "left.png";//http://res.cloudinary.com/pussyhunter/image/upload/c_scale,f_auto,h_350/left_handle_cup_i7ztfs.jpg"


    function loadUpperIMage(img1) {
        var img = new Image();
        img.src = img1;//"r.jpeg";//SelectedImage.result;// "2.png";//"http://res.cloudinary.com/pussyhunter/image/upload/v1488184107/500_F_97150423_M13q2FeAUZxxIx6CaPixHupprmyiVVli_skh6fe.jpg"
        //    img.src = "http://res.cloudinary.com/pussyhunter/image/upload/v1488184107/500_F_97150423_M13q2FeAUZxxIx6CaPixHupprmyiVVli_skh6fe.jpg"
        img.onload = function () {
            var iw = img.width;
            var ih = img.height;
            var xOffset = 102, //left padding
                    yOffset = 110; //top padding

            var a = 75.0; //image width
            var b = 10; //round ness

            var scaleFactor = iw / (4 * a);

            // draw vertical slices
            var counter = 0;
            ctx.globalAlpha = 0.9;
            for (var X = 0; X < iw; X += 1) {
                var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                ctx.globalAlpha = 0.9;
                //void ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
                ctx.drawImage(img, X * scaleFactor, 0, iw / 9, ih, X + xOffset, y + yOffset, 1, 174);
                ctx.globalCompositeOperation = "multiply";
                left = canvas.toDataURL({
                    format: 'jpeg',
                    quality: 0.8
                });
            }

        };
    }

}
;
function canvas2(img1) {

    var canvas = document.getElementById("canvas2");
    var ctx = canvas.getContext("2d");

    var productImg = new Image();
    productImg.onload = function () {
        var iw = productImg.width;
        var ih = productImg.height;

        canvas.width = iw;
        canvas.height = ih;

        ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                0, 0, iw, ih);
        loadUpperIMage(img1)
    };


    productImg.src = "center.jpg";//"http://res.cloudinary.com/pussyhunter/image/upload/h_350/canter_handle_cup_xyxhdd.jpg"
    var copycanvas = document.createElement('canvas')
    var copyctx = copycanvas.getContext('2d');
    var blankCenterImage = new Image();
    blankCenterImage.onload = function () {
        var iw = blankCenterImage.width;
        var ih = blankCenterImage.height;

        copycanvas.width = iw;
        copycanvas.height = ih;

        copyctx.drawImage(blankCenterImage, 0, 0, blankCenterImage.width, blankCenterImage.height,
                0, 0, iw, ih);
        // loadUpperIMage(img1)
    };
    blankCenterImage.src = "blank_center.png";
    function loadUpperIMage(img1) {
        var img = new Image();

        img.src = img1;//"argusoft.png";//"http://res.cloudinary.com/pussyhunter/image/upload/v1488184107/500_F_97150423_M13q2FeAUZxxIx6CaPixHupprmyiVVli_skh6fe.jpg"
        img.onload = function () {

            var iw = img.width;
            var ih = img.height;

            // alert(iw)

            var xOffset = 101, //left padding
                    yOffset = 110; //top padding

            var a = 75.0; //image width
            var b = 10; //round ness

            var scaleFactor = iw / (4 * a);

            // draw vertical slices
            for (var X = 0; X < iw; X += 1) {
                var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                ctx.globalAlpha = 0.9;
                ctx.drawImage(img, X * scaleFactor, 0, iw / 3, ih, X + xOffset, y + yOffset, 1, 174);
                ctx.globalCompositeOperation = "multiply";
                center = canvas.toDataURL({
                    format: 'jpeg',
                    quality: 0.8
                });

            }
//            setTimeout(function () {
//                if (canvas.toDataURL() === copycanvas.toDataURL()) {
//                    console.log("blank");
//                } else {
//                    console.log("Not blank");
//                }
//            }, 200);


//            function isCanvasTransparent(canvas) { // true if all pixels Alpha equals to zero
//                var ctx = canvas.getContext("2d");
//                var imageData = ctx.getImageData(0, 0, canvas.offsetWidth, canvas.offsetHeight);
//                for (var i = 0; i < imageData.data.length; i += 4)
//                    if (imageData.data[i + 3] !== 0)
//                        return false;
//                return true;
//            }
//            console.log(isCanvasTransparent(canvas));

        };
    }

}
;

function canvas3(img1) {

    var canvas = document.getElementById("canvas3");
    var ctx = canvas.getContext("2d");

    var productImg = new Image();
    productImg.onload = function () {
        var iw = productImg.width;
        var ih = productImg.height;

        canvas.width = iw;
        canvas.height = ih;

        ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                0, 0, iw, ih);
        loadUpperIMage(img1)
    };

    productImg.src = "right.jpg";//"http://res.cloudinary.com/pussyhunter/image/upload/h_350/right_handle_cup_dsdhr7.jpg"


    function loadUpperIMage(img1) {
        var img = new Image();

        img.src = img1;//"argusoft.png";//"http://res.cloudinary.com/pussyhunter/image/upload/v1488184107/500_F_97150423_M13q2FeAUZxxIx6CaPixHupprmyiVVli_skh6fe.jpg"

        img.onload = function () {

            var iw = img.width;
            var ih = img.height;

            //alert(iw)

            var xOffset = 102, //left padding
                    yOffset = 110; //top padding

            var a = 75.0; //image width
            var b = 10; //round ness

            var scaleFactor = iw / (3 * a);

            // draw vertical slices
            for (var X = 0; X < iw; X += 1) {
                var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                ctx.globalAlpha = 0.9;
                ctx.drawImage(img, X * scaleFactor, 0, iw / 1.5, ih, X + xOffset, y + yOffset, 1, 174);
                ctx.globalCompositeOperation = "multiply";
                right = canvas.toDataURL({
                    format: 'jpeg',
                    quality: 0.8
                });
            }

//            function pick(event) {
//                var x = event.layerX;
//                var y = event.layerY;
//                var pixel = ctx.getImageData(x, y, 1, 1);
//                var data = pixel.data;
//                var rgba = 'rgba(' + data[0] + ', ' + data[1] +
//                        ', ' + data[2] + ', ' + (data[3] / 255) + ')';
//               // color.style.background = rgba;
//               // color.textContent = rgba;
//               console.log(rgba);
//            }
//            canvas.addEventListener('mousemove', pick);
        };
    }

}
;